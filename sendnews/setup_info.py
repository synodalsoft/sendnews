SETUP_INFO = dict(
    name='sendnews',
    version='0.0.1',
    install_requires=['click', 'virtualenv', 'jinja2', 'distro', 'rstgen'],
    # tests_require=['docker', 'atelier'],
    # test_suite='tests',
    description="A command-line tool for sending news.",
    long_description="""
    """,
    author='Rumma & Ko Ltd',
    author_email='info@lino-framework.org',
    url="https://gitlab.com/lino-framework/sendnews",
    license_files=['COPYING'],
    entry_points={
        'console_scripts': ['sendnews = sendnews.cli:main'],
    },

    classifiers="""\
Programming Language :: Python :: 3
Development Status :: 1 - Planning
Environment :: Console
Framework :: Django
Intended Audience :: Developers
Intended Audience :: System Administrators
License :: OSI Approved :: GNU Affero General Public License v3
Operating System :: OS Independent
Topic :: System :: Installation/Setup
Topic :: Software Development :: Libraries :: Python Modules
""".splitlines())

SETUP_INFO.update(
    zip_safe=False,
    include_package_data=True)

SETUP_INFO.update(packages=[n for n in """
sendnews
""".splitlines() if n])
