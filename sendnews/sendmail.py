# -*- coding: utf-8 -*-
# Copyright 2002-2024 Rumma & Ko Ltd

USE_SSL = True
USE_STARTTLS = False
TIMEOUT = 5
ASK_DELIVERY_RECEIPT = False

import smtplib
import ssl
from pathlib import Path
#import string
import socket
# import sys
import os.path
opj=os.path.join
#import getopt
import getpass
import glob
import email
import mimetypes
import codecs
# import types

from email import encoders
from email.message import Message
from email.mime.audio import MIMEAudio
from email.mime.base import MIMEBase
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.header import Header
from email.utils import getaddresses, formataddr, parseaddr, formatdate

UsageError = Exception
OperationFailed = Exception

_ = lambda x: x

from optparse import OptionParser


class MyMessage(Message):

    def __init__(self):
        Message.__init__(self)
        """
        If I don't set transfer-encoding myself, set_charset()
        will set it to base64, which apparently is wrong
        """
        self.add_header('Content-Transfer-Encoding', '8bit')
        self.set_charset("utf-8")

    def set_payload(self, payload, charset=None):
        payload = payload.encode('utf-8')
        Message.set_payload(self, payload, self.get_charset())



class Sendmail:

    name = "Secure Sendmail"

    usage = "usage: %prog [options] FILE1 [FILE2 ...]"

    description="""\
sends an email stored in a FILE to a list of recipients
stored in a separate list file.
FILE is the input file (can be text or html).
"""

    def encodeaddrs(self,msg,hdr,recipients=None):
        all = msg.get_all(hdr, [])
        del msg[hdr]
        l=[]
        for name,addr in getaddresses(all):
                #name=msg.get_charset().header_encode(name)
                name=str(Header(str(name),msg.get_charset()))
                if recipients is not None:
                    recipients.append((name,addr))
                l.append(formataddr((name,addr)))
        msg[hdr] = Header(", ".join(l))
        return all

    def confirm(self, msg, *args):
        print(msg)
        input(_("Press ENTER to continue"))
        return True

    def notice(self, msg, *args):
        print(msg % args)

    def warning(self, msg, *args):
        print(msg % args)

    def debug(self, msg, *args):
        # print(msg % args)
        pass

    def main(self, *args, **kwargs):

        parser = OptionParser(usage=self.usage)
        parser.add_option("-s", "--subject",
                          help="the Subjet: line of the mail",
                          action="store",
                          type="string",
                          dest="subject",
                          default=None)

        ##         parser.add_option("-a", "--attach",
        ##                           help="add the specified FILE to the mail as attachment",
        ##                           action="callback",
        ##                           callback=self.attachfile,
        ##                           type="string",
        ##                           default=None)

        parser.add_option("-f", "--from",
                          help="the From: line (sender) of the mail",
                          action="store",
                          type="string",
                          dest="sender",
                          default=None)

        parser.add_option("-t", "--to",
                          help="""
        The To: line (recipient) of the mail.
        Taken from addrlist.txt if not given.
        """,
                          action="store",
                          type="string",
                          dest="recipient",
                          default=None)

        parser.add_option("-r", "--host",
                          help="the SMTP relay host",
                          action="store",
                          type="string",
                          dest="host",
                          default=None)

        parser.add_option("-u", "--user",
                          help="the username for the SMTP host",
                          action="store",
                          type="string",
                          dest="user",
                          default=None)

        parser.add_option("-p", "--password",
                          help="the password for the SMTP host",
                          action="store",
                          type="string",
                          dest="password",
                          default=None)
        parser.add_option("-e", "--encoding",
                          help="the encoding of the .eml input file",
                          action="store",
                          type="string",
                          dest="encoding",
                          default=None)

        self.options, self.args = parser.parse_args(*args, **kwargs)

        if len(self.args) == 0:
            raise UsageError("needs at least one argument")

        self.server = None
        self.dataDir = '.'
        self.count_ok = 0
        self.count_nok = 0


        if self.options.host is None:
            raise UsageError("--host must be specified")

##         for fn in  self.attach_files:
##             if not os.path.exists(fn):
##                 raise OperationFailed("File %s does not exist."%fn)

        files = []
        for pattern in self.args:
            files += glob.glob(pattern)

        self.count_todo = len(files)
        if self.count_todo == 0:
            self.notice("Nothing to do: no input files found.")
            return

        if self.options.recipient is None:
            recipients = []
        else:
            recipients = getaddresses([self.options.recipient])

        sender = self.options.sender
        subject = self.options.subject

        """

        if the first input file's name ends with .eml, then this
        becomes the outer message


        """
        if files[0].lower().endswith(".eml"):
            self.notice("Reading file %s...", files[0])
            # fp = codecs.open(files[0], "r", self.options.encoding)
            s = Path(files[0]).read_text(encoding=self.options.encoding)
            # print("="*80)
            # print(s)
            # print("="*80)
            first = email.message_from_string(s, _class=MyMessage)
            # fp = open(files[0], "r", encoding=self.options.encoding)
            # first = email.message_from_file(fp)
                # _class=MyMessage)
            if 'subject' in first:
                subject = first["subject"]
                del first["subject"]
                first["subject"] = Header(subject, first.get_charset())

            self.encodeaddrs(first, 'from')
            if sender is None: sender = first["from"]
            self.encodeaddrs(first, 'to', recipients)
            self.encodeaddrs(first, 'cc', recipients)
            self.encodeaddrs(first, 'bcc', recipients)
            del first['bcc']

            del files[0]
            self.count_todo -= 1
        else:
             first = None

        if len(files) == 0:
            outer = first
        else:
            # Create the enclosing (outer) message
            outer = MIMEMultipart()
            # outer.preamble = 'You will not see this in a MIME-aware mail reader.\n'
            if first is not None:
                first.add_header('Content-Disposition', 'inline')
                outer.attach(first)
                for hdr in ('to','cc'):
                    outer[hdr] = first[hdr]
            outer['subject'] = subject
            outer['from'] = sender
            self.notice("Attaching %d files...", self.count_todo)
            i = 1
            for filename in files:
                self.notice("%s (%d/%d)", filename, i, self.count_todo)
                part=self.file2msg(filename)
                # Set the filename parameter
                part.add_header('Content-Disposition', 'attachment',
                               filename=os.path.basename(filename))
                outer.attach(part)
                i += 1

        #for part in outer.walk():
            #if recipient is None: recipient=part["To"]
            #if bcc is None: bcc=part["Bcc"]

        if self.options.subject is not None:
            del outer['subject']
            outer['subject'] = self.options.subject
        #if self.options.sender is not None:
        #    del outer['from']
        #    outer['from'] = self.options.sender
        #del outer['to']
        #outer['to'] = recipient
        #if bcc is not None:
        #    outer['Bcc'] = bcc
        #    print "Bcc:", bcc

        #headers_i18n(outer)

        if len(recipients) == 0:
            for addr in open("addrlist.txt").xreadlines():
                addr = addr.strip()
                if len(addr) != 0 and addr[0] != "#":
                    recipients += getaddresses([addr])

        if not "Subject" in outer:
            raise "Subject header is missing"
        if not "Date" in outer:
            outer["Date"] = formatdate(None, True)

        for k,v in outer.items():
            #~ print k,":",str(v)
            self.notice("%s:%s", k, v)
        #self.notice(str(outer.keys()))
        self.notice(_("Message size: %d bytes."), len(str(outer)))
        self.notice(_("Send this to %d recipients: %s"),
                    len(recipients),
                    ", ".join([a[1] for a in recipients]))

        sender = parseaddr(str(sender))[1]

        if ASK_DELIVERY_RECEIPT:
            outer.add_header('Disposition-Notification-To', sender)

        # print "sender:", str(sender)

        # print outer.as_string(unixfrom=0)

        if False and not self.confirm("Okay?"):
            return

        self.connect()

        self.sendmsg(outer, sender, recipients)

        self.server.quit()

        self.notice(_("Sent to %d recipients."), self.count_ok)
        if self.count_nok != 0:
            self.notice(_("%d recipients refused."),self.count_nok)


    def file2msg(self, filename):
        if not os.path.exists(filename):
            raise OperationFailed("File %s does not exist." % filename)
        self.dataDir = os.path.dirname(filename)
        if len(self.dataDir) == 0:
            self.dataDir = "."

        (root, ext) = os.path.splitext(filename)

        ctype, encoding = mimetypes.guess_type(filename)

        if ctype is None or encoding is not None:
            # No guess could be made, or the file is encoded (compressed), so
            # use a generic bag-of-bits type.
            ctype = 'application/octet-stream'

        maintype, subtype = ctype.split('/', 1)

        if maintype == 'text':
            fp = open(filename)
            # Note: we should handle calculating the charset
            msg = MIMEText(fp.read(), _subtype=subtype)
            fp.close()
        elif maintype == 'image':
            fp = open(filename, 'rb')
            msg = MIMEImage(fp.read(), _subtype=subtype)
            fp.close()
        elif maintype == 'audio':
            fp = open(filename, 'rb')
            msg = MIMEAudio(fp.read(), _subtype=subtype)
            fp.close()
        else:
            fp = open(filename, 'rb')
            msg = MIMEBase(maintype, subtype)
            msg.set_payload(fp.read())
            fp.close()
            # Encode the payload using Base64
            encoders.encode_base64(msg)

        return msg

    def connect(self):

        if ":" in self.options.host:
            host, port = self.options.host.split(":")
            port = int(port)
        else:
            host = self.options.host
        self.notice("Connecting to %s", self.options.host)
        if USE_SSL:
            context = ssl.create_default_context()
            self.server = smtplib.SMTP_SSL(host, port, timeout=TIMEOUT, context=context)
        else:
            self.server = smtplib.SMTP(host, port, timeout=TIMEOUT)

        self.server.set_debuglevel(1)
        if USE_STARTTLS:
            self.server.starttls()
            self.server.ehlo()

        if self.options.user is None:
            self.notice("Using anonymous SMTP")
            return

        if self.options.password is None:
            self.options.password = getpass.getpass(
                'Password for %s@%s : ' % (self.options.user,
                                           self.options.host))

        self.server.login(self.options.user, self.options.password)


    def sendmsg(self, msg, sender, recipients):
        self.debug("sendmsg(%r,%r,%r)", msg, sender, recipients)

        #print msg.get_all("from")

        # note : simply setting a header does not overwrite an existing
        # header with the same key!

        #del msg["To"]
        #msg["To"] = toAddr


        #if self.options.sender is None:
        #    sender = msg['From'] # .encode('latin1')
        #else:
        #    sender = self.options.sender

        # body = str(msg)
        # body = msg
        body = msg.as_string(unixfrom=0)
        # body = msg.as_bytes(unixfrom=0)
        # print body
        mail_options = "" # "8bitmime"
        try:
            refused = self.server.sendmail(
                sender, [formataddr(a) for a in recipients],
                body, mail_options)
            # self.notice(
            #     u"Sent '%s' at %s to %s",
            #     msg["Subject"], msg["Date"], ", ".join(recipients))
            self.count_ok += len(recipients)
            #self.debug("=" * 80)
            #self.debug(body.decode('utf8'))
            #self.debug("=" * 80)
            if len(refused) > 0:
                for i in refused.items():
                    self.warning("refused %s : %s", *i)
                self.count_ok -= len(refused)
                self.count_nok += len(refused)
            return

        except smtplib.SMTPRecipientsRefused as e:
            self.error("%s : %s", recipients,e)
            # All recipients were refused. Nobody got the mail.
        except smtplib.SMTPHeloError as e:
            self.error("%s : %s", recipients,e)
        except smtplib.SMTPServerDisconnected as e:
            self.error("%s : %s", recipients,e)
        except smtplib.SMTPSenderRefused as e:
            self.error("%s : %s", sender,e)
        except smtplib.SMTPDataError as e:
            self.error("%s : %s", recipients,e)

        self.count_nok += len(recipients)
        return

##  def ToUserLog(self,msg):
##      try:
##          f = open("%s/user.log" % self.dataDir,"a")
##          f.write(str(msg)+"\n")
##          print msg
##      except IOError:
##          print "[no logfile] " + msg


if __name__ == '__main__':
    Sendmail().main()
