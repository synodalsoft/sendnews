# Copyright 2021 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

"""The main entry point for the :command:`sendnews` command.
"""

import click
import distro

from .setup_info import SETUP_INFO

from .send import send

@click.group(help="""
{description}
See {url} for more information.

This is {name} version {version}.
""".format(**SETUP_INFO))
def main():
    pass

main.add_command(send)

if __name__ == '__main__':
    main()
    # main(auto_envvar_prefix='GETLINO')
