# Copyright 2021 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

import os
import shutil
import secrets
import click

from os.path import join

import smtplib
#import string
import socket
# import sys
import os.path
opj=os.path.join
#import getopt
import getpass
import glob
import email
import mimetypes
import codecs
# import types

from email import encoders
from email.message import Message
from email.mime.audio import MIMEAudio
from email.mime.base import MIMEBase
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.header import Header
from email.utils import getaddresses, formataddr, parseaddr, formatdate

def _(s):
    return s

class unused_MyMessage(Message):

    def __init__(self):
        Message.__init__(self)
        """
        If I don't set transfer-encoding myself, set_charset()
        will set it to base64 which apparently is wrong
        """
        self.add_header('Content-Transfer-Encoding','8bit')
        self.set_charset("utf-8")


    def set_payload(self, payload, charset=None):
        payload=payload.encode('utf-8')
        Message.set_payload(self,payload,self.get_charset())


@click.command()
@click.argument('filename')
@click.option('--subject', '-s', help="the Subjet line of the mail")
@click.option('--sender', '-f', help="the From line of the mail")
@click.option('--host', '-h', default="localhost", help="SMTP host")
@click.option('--username', '-u', help="username for SMTP host")
@click.option('--password', '-p', help="password for user on SMTP host")
@click.pass_context
def send(ctx, *args, **kwargs):
    """

    Send the email stored in the .eml file to a list of recipients
    stored in a separate list file.

    """
    App(*args, **kwargs).run()


class AttrDict(dict):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__dict__ = self



class App:

    def __init__(self, filename, **options):
        # subject=None, host=None, username=None, password=None
        super().__init__()
        self.filename = filename
        self.options = AttrDict(options)

        self.server = None
        self.dataDir = '.'
        self.count_ok = 0
        self.count_nok = 0

    def encodeaddrs(self, msg, hdr, recipients=None):
        all = msg.get_all(hdr, [])
        del msg[hdr]
        l = []
        for name, addr in getaddresses(all):
            #name=msg.get_charset().header_encode(name)
            name = str(Header(name, msg.get_charset()))
            if recipients is not None:
                recipients.append((name,addr))
            l.append(formataddr((name,addr)))
        msg[hdr] = Header(", ".join(l))
        return all

    def run(self):

        print("Options:", self.options)

        recipients = []

        sender = self.options.sender
        subject = self.options.subject


        click.echo("Reading file %s..." % self.filename)
        first = email.message_from_file(open(self.filename, "r"))
        if 'subject' in first:
            subject = first["subject"]
            del first["subject"]
            first["subject"] = Header(subject, first.get_charset())

        self.encodeaddrs(first,'from')
        if sender is None: sender = first["from"]
        self.encodeaddrs(first, 'to', recipients)
        self.encodeaddrs(first, 'cc', recipients)
        self.encodeaddrs(first, 'bcc', recipients)
        del first['bcc']
        outer = first

        if self.options.subject is not None:
            del outer['subject']
            outer['subject'] = self.options.subject
        #if self.options.sender is not None:
        #    del outer['from']
        #    outer['from'] = self.options.sender
        #del outer['to']
        #outer['to'] = recipient
        #if bcc is not None:
        #    outer['Bcc'] = bcc
        #    print "Bcc:", bcc

        #headers_i18n(outer)

        recipients = []
        for addr in open(opj(self.dataDir, "addrlist.txt")).readlines():
            addr = addr.strip()
            if len(addr) != 0 and addr[0] != "#":
                recipients += getaddresses([addr])

        if not "Subject" in outer:
            raise Exception("Subject header is missing")
        # if not "Date" in outer:
        #     outer["Date"] = formatdate(None, True)

        for k, v in outer.items():
            #~ print k,":",unicode(v)
            click.echo("%s:%s" % (k,v))
        #click.echo(str(outer.keys()))
        click.echo("Message size: %d bytes." % len(str(outer)))
        click.echo("Send this to %d recipients: %s" % (
            len(recipients), ", ".join([a[1] for a in recipients])))

        sender = parseaddr(sender)[1]

        # print "sender:", unicode(sender)

        # print outer.as_string(unixfrom=0)

        if not click.confirm("Okay?"):
            return

        self.connect()

        self.sendmsg(outer, sender, recipients)

        self.server.quit()

        click.echo("Sent to %d recipients." % self.count_ok)
        if self.count_nok != 0:
            click.echo("%d recipients refused." % self.count_nok)


    def file2msg(self,filename):
        if not os.path.exists(filename):
            raise OperationFailed("File %s does not exist." % filename)
        self.dataDir = os.path.dirname(filename)
        if len(self.dataDir) == 0:
            self.dataDir = "."

        (root,ext) = os.path.splitext(filename)

        ctype, encoding = mimetypes.guess_type(filename)

        if ctype is None or encoding is not None:
            # No guess could be made, or the file is encoded (compressed), so
            # use a generic bag-of-bits type.
            ctype = 'application/octet-stream'

        maintype, subtype = ctype.split('/', 1)

        if maintype == 'text':
            fp = open(filename)
            # Note: we should handle calculating the charset
            msg = MIMEText(fp.read(), _subtype=subtype)
            fp.close()
        elif maintype == 'image':
            fp = open(filename, 'rb')
            msg = MIMEImage(fp.read(), _subtype=subtype)
            fp.close()
        elif maintype == 'audio':
            fp = open(filename, 'rb')
            msg = MIMEAudio(fp.read(), _subtype=subtype)
            fp.close()
        else:
            fp = open(filename, 'rb')
            msg = MIMEBase(maintype, subtype)
            msg.set_payload(fp.read())
            fp.close()
            # Encode the payload using Base64
            encoders.encode_base64(msg)

        return msg



    def connect(self):

        try:
            click.echo("Connecting to %s" % self.options.host)
            self.server = smtplib.SMTP(self.options.host)
        except socket.error as e:
            raise OperationFailed(
                "Could not connect to %s : %s" % (
                self.options.host,e))
        except socket.gaierror as e:
            raise OperationFailed(
                "Could not connect to %s : %s" % (
                self.options.host,e))

        # server.set_debuglevel(1)

        if self.options.user is None:
            click.echo("Using anonymous SMTP")
            return

        if self.options.password is None:
            self.options.password = getpass.getpass(
                'Password for %s@%s : ' % (self.options.user,
                                           self.options.host))

        try:
            self.server.login(self.options.user,self.options.password)
            return

        except Exception as e:
            raise OperationFailed(str(e))

##         except smtplib.SMTPHeloError,e:
##             self.ui.error(
##                 "The server didn't reply properly to the 'HELO' greeting: %s", e)
##         except smtplib.SMTPAuthenticationError,e:
##             self.ui.error(
##                 "The server didn't accept the username/password combination: %s",e)
##         except smtplib.SMTPException,e:
##             self.ui.error(str(e))
##         return False


    def sendmsg(self,msg,sender,recipients):
        self.debug("sendmsg(%r,%r,%r)",msg,sender,recipients)

        #print msg.get_all("from")

        # note : simply setting a header does not overwrite an existing
        # header with the same key!

        #del msg["To"]
        #msg["To"] = toAddr


        #if self.options.sender is None:
        #    sender = msg['From'] # .encode('latin1')
        #else:
        #    sender = self.options.sender

        # body = str(msg)
        body = msg.as_string(unixfrom=0)
        # print body
        mail_options="" # "8bitmime"
        try:
            refused = self.server.sendmail(sender,
                                           [formataddr(a) for a in recipients],
                                           body,mail_options)
            # click.echo(
            #     u"Sent '%s' at %s to %s",
            #     msg["Subject"], msg["Date"], ", ".join(recipients))
            self.count_ok += len(recipients)
            #self.debug("=" * 80)
            #self.debug(body.decode('utf8'))
            #self.debug("=" * 80)
            if len(refused) > 0:
                for i in refused.items():
                    self.warning(u"refused %s : %s", *i)
                self.count_ok -= len(refused)
                self.count_nok += len(refused)
            return

        except smtplib.SMTPRecipientsRefused as e:
            self.error("%s : %s", recipients,e)
            # All recipients were refused. Nobody got the mail.
        except smtplib.SMTPHeloError as e:
            self.error("%s : %s", recipients,e)
        except smtplib.SMTPServerDisconnected as e:
            self.error("%s : %s", recipients,e)
        except smtplib.SMTPSenderRefused as e:
            self.error("%s : %s", sender,e)
        except smtplib.SMTPDataError as e:
            self.error("%s : %s", recipients,e)

        self.count_nok += len(recipients)
        return
